# ansible
* *Its a provisioning and configuration management Tool*
* *server configuration and infrastructure configuration*
* *Ansible support push based machanisum and worked on 22 port number*
* *Ansible devloped with python so python is the platform to use ansible, it uses python modules*
* *Ansible modules are nothing but ansible commands*
## How does it work?
Ansible connects nodes, pushes small programs called modules to the nodes, and then removes them when they are done.
![Screenshot](Ansible_How-does-it-work.png)

## installation
```shell
yum install epel-release -y     # thirdparty reposetory
yum install ansible -y          # install ansible
ansible --version               # check ansible version
```
### ansible works on simple ssh port 22, work similar to ssh 
```shell
ssh -i "id_rsa" centos@IP       # ssh command to get access, i 
ansible -i host all -u centos --private-key=./id_rsa -m shell - a hostname
# -i host all Inventory list
# host is a inventory file having all webservers information(IPs)
# -u =  user name
# --private-key= set access key path
# -m = ansible module
# shell is the ansible module = ansible command you can say
# -a -- argument 
# -e --- environment or varible
# -v --- detailed info of errors
# hostname linux command to check connection
/etc/ansible    # ansible main directory
/etc/ansible/hosts # host inventory file having all web servers info(list of IPs)
/etc/ansible/ansible.cfg # main ansible configuration file

#if you are installing ansible using python then there is configuration file ansible.cfg not having the complete configuration inside it. 
#So we have to create a cansible.cfg file configuration.
ansible-config init --disabled -t all > ansible.cfg         #command to create ansible.cfg configuration. 

```
## ansible playbooks
ansible support YAML language, and YAML sopport following datatypes
1. string           "abc123"
2. number           "12345"
3. Boolean          "true, false, 0 1,"
4. list []          ["abc123", "bcd323"] or 
                    - abc123
                    - bcd323
5. map {    
    address: 
        city: pune
        area: nal stop
        pin code: 411004
}
6. list of maps
7. map of list
## variables
* It is an intity used to store values, thats values we can use repetedly. 
* Its a number or charector that store values.
------------------------------------------
* issues we are facing in normal work.
    * repetative task
    * human error
    * multiple file - multiple values.
* So to resolved this issues we are using variable.     
* variable phases--> initalizing --> define --> call

### main varibles
1. global variable
2. local variable

### priority based variables
1. cli var  
* ansible-playbook -e (variable=value) (playbook name)
2. local var
3. file var
4. prompt var 
5. global var
6. host var

* registor variable --- to store values from one task and pass it to other task
works as pipe command (output >| input> )

## Ansible Modules
1. debug -- its similar to echo command, used print massage on terminal
2. shell -- to execute linux commands
3. default task - gathering facts
IT will gather all information about host servers in JSON format.
```shell
ansible all -m setup #to gather facts manually, all information about host servers
```
We can call facts values in playbooks. 

4. Ignore errors        -- it used to ignor less priority tasks
5. conditions: (when)   -- it is used to apply specific conditions, we can apply multiple condition using "and" and "or"
6. priviliage -(become) -- it used provide root priviliage
7. loop                 -- it used for repetative tasks
8. setfacts             -- to set own facts, similar to variable
9. yum, apt             -- to install 
10. package             -- it will auto select yum or apt as per OS family
11. systemd or service  -- to start services
12. Tags                --  lables to identify tasks
```shell
--list-tags     # To list all tags given in playbook
--skip-tag      # to skip specific task with given tags
--tags          # To execute only task with given tags
```
13. copy                --  To copy files from ansible server to host server
14. lineinfile          -- To add or replace single line in a file
15. blockinfile         -- To add block of multiple lines in file

